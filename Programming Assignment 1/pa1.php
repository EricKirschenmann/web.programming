<DOCTYPE html>
<html>
	<!--
		TODO:
		1. allow only one input: just convert the single value
		2. error checking
	-->

	<head>
	<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700' rel='stylesheet' type='text/css'>
		<style type="text/css">
			table, th, td {
			    border: 1px solid black;
			    border-collapse: collapse;
				font-family: inconsolata;
			}
			th, td {
			    padding: 5px;
			    width: 150px;
			}
			label {
				float: left;
				width: 200px;
			}
		</style>
	</head>

	<body>
		<!-- Form that collects the temperatures -->
		<form action="pa1.php" method="post">
			<div>
			<label for="temperature1"> First Temperature: </label>
			<input type="text" name="temp1" value="0"/>
			</div>
			
			<div>
			<label for="temperature2">Second Temperature: </label>
			<input type="text" name="temp2" value="0"/>
			<div>

			<div>
			<label for="unit_select">What is the starting unit?</label>
			<select name="units" required>
				<option value="Fahrenheit" selected>Fahrenheit</option>
				<option value="Celsius">Celsius</option>
				<option value="Kelvin">Kelvin</option>
			</select>
			</div>
			
			</br>
			
			<div>
			<input type="submit" value="convert">
			</div>

		</form>

		<br>
		</br>
		
		<div>
			<!-- PHP that converts and displays the temperatures -->
			<?php
				/////////////////////////////////////////////////////////////
				///	MAIN CODE THAT GENERATES A TABLE BASED ON TWO INPUTS ///
				///////////////////////////////////////////////////////////
				if(isset($_POST["temp1"]) && isset($_POST["temp2"]))
				{
					//Global variables
					$temp1 = $_POST["temp1"];
					$temp2 = $_POST["temp2"];
					$current = $temp1;
					$units = substr($_POST["units"], 0, 1); //store the first char of the units
					$firstTemp = TRUE;
					$reverse = FALSE;

					//Global variables storing the current temperatures
					$Fahrenheit = 0;
					$Celsius = 0;
					$Kelvin = 0;

					if($temp1 > $temp2)
					{
						$length = $temp1 - $temp2;
						$reverse = TRUE;
					}
					elseif ($temp1 < $temp2) 
					{
						$length = $temp2 - $temp1;
					}
					else
					{
						$length = 0;
					}

					//Functions that convert to the different units
					function convert()
					{
						global $Fahrenheit, $Celsius, $Kelvin, $units, $current;

						if($units == "C")
						{
							$Celsius = $current;
							toFahrenheit();
							toKelvin();

							$Celsius = number_format($Celsius,2);
						}
						elseif($units == "F")
						{
							$Fahrenheit = $current;
							toCelsius();
							toKelvin();

							$Fahrenheit = number_format($Fahrenheit,2);
						}
						elseif($units == "K")
						{
							$Kelvin = $current;
							toCelsius();
							toFahrenheit();

							$Kelvin = number_format($Kelvin,2);
						}
					}

					function toCelsius()
					{
						global $Fahrenheit, $Celsius, $Kelvin, $units;

						if($units == "K")
							$Celsius = $Kelvin - 273.15;
						else
							$Celsius = ($Fahrenheit - 32) * (5 / 9);

						$Celsius = number_format($Celsius,2);
					}

					function toFahrenheit()
					{
						global $Fahrenheit, $Celsius, $Kelvin, $units;

						$Fahrenheit = ($Celsius * (9 / 5)) + 32;
						$Fahrenheit = number_format($Fahrenheit,2);
					}

					function toKelvin()
					{
						global $Fahrenheit, $Celsius, $Kelvin, $units, $reverse;

						$Kelvin = $Celsius + 273.15;
						$Kelvin = number_format($Kelvin,2);
					}

					function nextTemp()
					{
						global $firstTemp, $current, $temp1, $reverse;

						if($firstTemp)
						{
							$current = $temp1;
							$firstTemp = FALSE;
						}
						else
						{
							if(!$reverse)
								$current++;
							else
								$current--;
						}
					}

					echo "<table>";
						echo "<tr>";
							echo "<th>Fahrenheit</th>";
							echo "<th>Celsius</th>";
							echo "<th>Kelvin</th>";
						echo "</tr>";
						for($x = 0; $x <= $length; $x++)
						{
							nextTemp();
							convert();
							echo "<tr>";
								echo "<td>$Fahrenheit °F</td>";
								echo "<td>$Celsius °C</td>";
								echo "<td>$Kelvin K</td>";
							echo "</tr>";	
						}
					echo "</table>";


				}

			?>

		</div>
	</body>
</html>