<DOCTYPE html>
<html>
	<!--
		Programming Assignment 1
		CS363 - Web Programming 
		Eric Kirschenmann
		Purpose:	Create a basic webpage that utilizes PHP to take in temperatures
					and display their conversions to Celsius and Kelvin.
	-->

	<!-- Where all formatting and css is declared -->
	<head>
	<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700' rel='stylesheet' type='text/css'>
		<style type="text/css">
			table, th, td {
			    border: 1px solid black;
			    border-collapse: collapse;
				font-family: inconsolata;
			}
			th, td {
			    padding: 5px;
			    width: 150px;
			}
			td {
				text-align: right;
			}
			label {
				float: left;
				width: 200px;
			}
		</style>
	</head>

	<body>


	<!--For the sticky form this creates default values that the page can use
		if they are not already set, and if they are it retreives the values and sets them -->
		<?php
			$fTemp = '';
			$lTemp = '';
			$cUnits = 'Fahrenheit';

			if(isset($_POST["submit"])) {
				$fTemp = $_POST["temp1"];
				$lTemp = $_POST["temp2"];
				$cUnits = $_POST["units"];
				$output_form = FALSE;
			}
		?>

		<!--Form that collects the temperatures,
			uses the post method of sending and retrieving data
			and then uses php to pull the already sent data if it exists. -->
		<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
			<div>
			<label for="temperature1"> First Temperature: </label>
			<input type="text" name="temp1" value="<?php echo $fTemp ?>"/>
			</div>
			
			<div>
			<label for="temperature2">Second Temperature: </label>
			<input type="text" name="temp2" value="<?php echo $lTemp ?>"/>
			<div>

			<div>
			<label for="unit_select">What is the starting unit?</label>
			<select name="units" required>
				<option value="Fahrenheit" <?php if($cUnits == "Fahrenheit") echo 'selected';?> >Fahrenheit</option>
				<option value="Celsius" <?php if($cUnits == "Celsius") echo 'selected';?> >Celsius</option>
				<option value="Kelvin" <?php if($cUnits == "Kelvin") echo 'selected';?> >Kelvin</option>
			</select>
			</div>
			
			</br>
			
			<div>
			<input type="submit" name = "submit" value="convert">
			</div>

		</form>

		<br>
		</br>
		
		<div>
			<!-- PHP that converts and displays the temperatures -->
			<?php
				/////////////////////////////////////////////////////////////
				///	MAIN CODE THAT GENERATES A TABLE BASED ON TWO INPUTS ///
				///////////////////////////////////////////////////////////
				if(isset($_POST["temp1"]) && isset($_POST["temp2"]))
				{
					//Global variables
					$temp1 = $_POST["temp1"];
					$temp2 = $_POST["temp2"];
					$current = $temp1;
					$units = substr($_POST["units"], 0, 1); //store the first char of the units
					$firstTemp = TRUE;
					$reverse = FALSE;

					//Global variables storing the current temperatures
					$Fahrenheit = 0;
					$Celsius = 0;
					$Kelvin = 0;

					//check which direction the table will be created
					//by checking if the starting temp is higher or lower
					//than the ending temperature and also creating the length of the table
					if($temp1 > $temp2)
					{
						$length = $temp1 - $temp2;
						$reverse = TRUE;
					}
					elseif ($temp1 < $temp2) 
					{
						$length = $temp2 - $temp1;
					}
					else
					{
						$length = 0;
					}

					//Function that calls all the convert methods in order based
					//on the starting units as well as format the output
					function convert()
					{
						global $Fahrenheit, $Celsius, $Kelvin, $units, $current;

						if($units == "C")
						{
							$Celsius = $current;
							toFahrenheit();
							toKelvin();

							$Celsius = number_format($Celsius,2);
						}
						elseif($units == "F")
						{
							$Fahrenheit = $current;
							toCelsius();
							toKelvin();

							$Fahrenheit = number_format($Fahrenheit,2);
						}
						elseif($units == "K")
						{
							$Kelvin = $current;
							toCelsius();
							toFahrenheit();

							$Kelvin = number_format($Kelvin,2);
						}
					}

					//function that converts the starting temperature
					//to Celsius based on the starting units and formats the output
					function toCelsius()
					{
						global $Fahrenheit, $Celsius, $Kelvin, $units;

						if($units == "K")
							$Celsius = $Kelvin - 273.15;
						else
							$Celsius = ($Fahrenheit - 32) * (5 / 9);

						$Celsius = number_format($Celsius,2);
					}
					//function that converts the starting temperature
					//to Fahrenheit based on the starting units and formats the output
					function toFahrenheit()
					{
						global $Fahrenheit, $Celsius, $Kelvin, $units;

						$Fahrenheit = ($Celsius * (9 / 5)) + 32;
						$Fahrenheit = number_format($Fahrenheit,2);
					}
					//function that converts the starting temperature
					//to Kelvin based on the starting units and formats the output
					function toKelvin()
					{
						global $Fahrenheit, $Celsius, $Kelvin, $units, $reverse;

						$Kelvin = $Celsius + 273.15;
						$Kelvin = number_format($Kelvin,2);
					}

					//that generates the next temperature to be converted
					//for the table, called for each iteration of the for-loop
					function nextTemp()
					{
						global $firstTemp, $current, $temp1, $reverse;

						if($firstTemp)
						{
							$current = $temp1;
							$firstTemp = FALSE;
						}
						else
						{
							//check for the direction of the table
							if(!$reverse)
								$current++;
							else
								$current--;
						}
					}


					//Use a for-loop to generate a table that
					//can be any length by dynamically adding
					//rows based on the size distance between 
					//start and end temperatures.
					echo "<table>";
						echo "<tr>";
							echo "<th>Fahrenheit</th>";
							echo "<th>Celsius</th>";
							echo "<th>Kelvin</th>";
						echo "</tr>";
						for($x = 0; $x <= $length; $x++)
						{
							nextTemp(); //get the next temperature
							convert();	//convert the temperature
							echo "<tr>";
								echo "<td>$Fahrenheit °F</td>";
								echo "<td>$Celsius °C</td>";
								echo "<td>$Kelvin K</td>";
							echo "</tr>";	
						}
					echo "</table>";


				}

			?>

		</div>
	</body>
</html>