<DOCTYPE html>
<html>
	<!--
		Programming Assignment 2
		CS363 - Web Programming 
		Eric Kirschenmann
		Purpose:	Basic web program that checks the validity of an entered password.
	-->

	<!-- Where all formatting and css is declared -->
	<head>
	<link href='http://fonts.googleapis.com/css?family=Inconsolata:400,700' rel='stylesheet' type='text/css'>		
	<title>Password Checker</title>
		<style type="text/css">
			table, th, td {
			    border: 1px solid black;
			    border-collapse: collapse;
				font-family: inconsolata;
			}
			th, td {
			    padding: 5px;
			    width: 150px;
			}
			td {
				text-align: right;
			}
			label {
				float: left;
				width: 200px;
			}
		</style>
	</head>

	<body>

	<!--Set default page values with sticky form-->
		<?php
			$pass = '';

			if(isset($_POST["submit"])) {
				$pass = $_POST["pass"];
				$output_form = FALSE;
			}
		?>

		<!--Format the page with the header as well as the form that takes in input-->
		<header>
			<h1>Password Checker</h1>
		</header>

		<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
			<div>
			<label for="password">Enter a password: </label>
			<input type="text" name="pass" value="<?php echo $pass ?>"/>
			</div>
			
			<div>
			<input type="submit" name = "submit" value="check">
			</div>

		</form>

		<?php

		/**
		* Rule 1: Valid Length - The length of the password must be 8 to 16 only
		* Rule 2: No Space - The password must not contain any space or blank character.
		* Rule 3: At least 2 digits - The password must conatian at least 2 digits.
		* Rule 4: At least 1 upper-case letter - The password must conatain at least one upper-case letter.
		* Rule 5: At least 1 lower-case letter – The password must contain at least one lower-case letter.
		* Rule 6: At least 1 special character – The password must contain at least one special character, which can be one of the following 7 choices: ‘$’, ‘#’, ‘@’, ‘&’, ‘*’, ‘?’, or ‘!’.
		* Rule 7: No special numbers – The password must not contain any of the following 6 numbers: 2016, 2015, 2014, 2013, 2012, or 2011. 
		**/
			if(isset($_POST["pass"]))
			{
				//store the value entered into the text field
				$password = $_POST["pass"];
				/**
				* Variables for all the different rules
				**/
				$validLength = false;
				$numSpaces = 0;
				$numDigits = 0;
				$numUppers = 0;
				$numLowers = 0;
				$numSpecials = 0;
				$containsDate = false;
				$validPassword = false;

				/**
				* Function that takes the password string and checks it based on the different rules
				**/
				function checkValidity()
				{
					//declare the global variables
					global $password, $validLength, $numSpaces, $numDigits, $numUppers, $numLowers, $numSpecials, $containsDate;

					//check if the length of the password is valid
					if(strlen($password) >= 8 && strlen($password) <= 16)
					{
						$validLength = true;
					}

					//split the string into an array of chars
					$charArray = str_split($password);

					//for loop that checks all the different rules given
					//checks each char individually and increments the given
					//variable depending on which check
					foreach ($charArray as $key) 
					{
						//check for space
						if($key == " ")
						{
							$numSpaces = $numSpaces + 1;
						}
						//check for digits
						elseif(ctype_digit($key))
						{
							$numDigits = $numDigits + 1;
						}
						//check for upper-case
						elseif(ctype_upper($key))
						{
							$numUppers = $numUppers + 1;
						}
						//check for lower-case
						elseif(ctype_lower($key))
						{
							$numLowers = $numLowers + 1;
						}
						//check for the special characters
						elseif($key == "$" || $key == "#" || $key == "@" || $key == "&" || $key == "*" || $key == "?" || $key == "!")
						{
							$numSpecials = $numSpecials + 1;
						}

					}

					//check for any of the special dates within the password
					for($x = 2011; $x <= 2016; $x++)
					{
						if(strpos($password, strval($x)) != false)
						{
							$containsDate = true;
						}
					}

					/* print out the values after checking the password for debugging purposes.
					echo "length of password: $validLength<br>";
					echo "number of spaces: $numSpaces<br>";
					echo "number of digits: $numDigits<br>";
					echo "number of uppers: $numUppers<br>";
					echo "number of lowers: $numLowers<br>";
					echo "number of specials: $numSpecials<br>";
					echo "containsDate: $containsDate<br>";*/

				}

				/**
				* Function that takes all of the calculated values and checks for validity
				**/
				function showValidity()
				{
					//declare the global variables needed
					global $password, $validLength, $numSpaces, $numDigits, $numUppers, $numLowers, $numSpecials, $containsDate, $validPassword;
					//if the variable is 1 the password is valid, so start with a valid password
					$passBool = 1; 

					//output error if the length is invalid
					if($validLength == false)
					{
						//if this is the first error print title (this is the same for all checks)
						if($passBool == 1)
							echo "<B>The password \"$password\" violates the following rule(s): </B><br>";
						$passBool = 0;
						echo "Rule 1: Valid Length – The length of the password must be 8 to 16 only.<br>";
					}
					//output error if password contains a space
					if($numSpaces >= 1)
					{
						if($passBool == 1)
							echo "<B>The password \"$password\" violates the following rule(s): </B><br>";
						$passBool = 0;
						echo "Rule 2: No Space – The password must not contain any space or blank character.<br>";
					}
					//output error if password doesn't contain two digits
					if($numDigits < 2)
					{
						if($passBool == 1)
							echo "<B>The password \"$password\" violates the following rule(s): </B><br>";
						$passBool = 0;
						echo "Rule 3: At least 2 digits – The password must contain at least 2 digits.<br>";
					}
					//output error if password doesn't contain an upper-case letter
					if($numUppers < 1)
					{
						if($passBool == 1)
							echo "<B>The password \"$password\" violates the following rule(s): </B><br>";
						$passBool = 0;
						echo "Rule 4: At least 1 upper-case letter – The password must contain at least one upper-case letter.<br>";
					}
					//output error if password doesn't contain a lower-case letter
					if($numLowers < 1)
					{
						if($passBool == 1)
							echo "<B>The password \"$password\" violates the following rule(s): </B><br>";
						$passBool = 0;
						echo "Rule 5: At least 1 lower-case letter – The password must contain at least one lower-case letter.<br>";
					}
					//output error if password doesn't contain a special character
					if($numSpecials < 1)
					{
						if($passBool == 1)
							echo "<B>The password \"$password\" violates the following rule(s): </B><br>";
						$passBool = 0;
						echo "Rule 6: At least 1 special character – The password must contain at least one special character, which can be one of the following 7 choices: ‘$’, ‘#’, ‘@’, ‘&’, ‘*’, ‘?’, or ‘!’.<br>";
					}
					//output error if password contains special numbers
					if($containsDate == true)
					{
						if($passBool == 1)
							echo "<B>The password \"$password\" violates the following rule(s): </B><br>";
						$passBool = 0;
						echo "Rule 7: No special numbers – The password must not contain any of the following 6 numbers: 2016, 2015, 2014, 2013, 2012, or 2011.<br>";
					}

					//if the password passes all the checks output a congratulations
					if($passBool == 1)
					{
						echo "<B>Congratulations! Your password \"$password\" is very secure!</B><br>";
					}

				}


				//call the functions
				checkValidity();
				showValidity();

			}
		?>


	</body>
</html>